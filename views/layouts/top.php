<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Wetterstation</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="/php42/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="/php42/css/index.css" rel="stylesheet">

    <script src="/php42/js/jquery.min.js"></script>
    <script src="/php42/js/bootstrap.min.js"></script>

    <!--  TODO Einbinden von chart.js -->
    <!--  TODO Einbinden von index.js -->

</head>
<body>
